var express = require('express');
var http = require('http');
var path = require('path');
var config = require('config');
//var log = require('lib/log')(module);
var db = require('db');
var HttpError = require('error').HttpError;
var errorhandler = require('errorhandler');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

http.createServer(app).listen(config.get('app_port'), function(){
    console.log('Express server listening on port ' + config.get('app_port'));
});

db.openDatabase(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('Database connection successful');
});

// Middleware

var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var pgSession = require('connect-pg-simple')(session);
//var SessionStore = require('express-mysql-session');
//var sessionStore = new SessionStore({}, db.connection);
var sessionStore = new pgSession({
    pg : db.pg,
    conString : db.conString,
    tableName : 'session'
});

app.use(favicon(__dirname + '/public/favicon.ico'));

/*if (app.get('env') == 'development') {
    app.use(express.logger('dev'));
} else {
    app.use(express.logger('default'));
}*/

app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret: config.get('session:secret'),
    key: config.get('session:key'),
    cookie: config.get('session:cookie'),
    store: sessionStore
}));

app.use(require('middleware/sendHttpError'));
app.use(require('middleware/loadUser'));
app.use(express.static(path.join(__dirname, 'public')));
require('routes')(app);
//app.use(require('routes'));

app.use(function(err, req, res, next) {
    if (typeof err == 'number') { // next(404);
        err = new HttpError(err);
    }

    if (err instanceof HttpError) {
        res.sendHttpError(err);
    } else {
        if (app.get('env') == 'development') {
            //express.errorHandler()(err, req, res, next);
            app.use(errorhandler(err, req, res, next));
        } else {
            console.log(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }
});

//module.exports = app;
