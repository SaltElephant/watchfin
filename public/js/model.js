/**
 * Created by Admin on 24.06.15.
 */
var Account = Backbone.Model.extend({
    defaults: {
        id:"",
        name: "",
        sum: "",
        cur:"",
        selectedacc: false
    },
    idAttribute: "id"
});
var AccountsCollection = Backbone.Collection.extend({
   model:Account,
   url: "/accounts"
});

var Transaction = Backbone.Model.extend({
    defaults: {
        id:"",
        name: "",
        sum: "",
        category:"",
        date:""
    },
    idAttribute: "id"
});

var TransactionsCollection = Backbone.Collection.extend({
    model:Transaction,
    url: "/transactions"
});


var AccountsHandler = Backbone.Model.extend({
    accounts : AccountsCollection,
    transactions : TransactionsCollection,
    selectedaccid : "",
    initialize: function(accs,trans,transview){
        accounts = accs;
        transactions = trans;
    },

    ChangeSelect: function(model){
        for (var i=0; i<accounts.length;i++ ){
            if (accounts.at(i) === model){
                selectedaccid = model.get('id')
            }else{
                accounts.at(i).set({selectedacc: false},{silent: true})
            }
        }
        transactions.fetch({
            data: $.param({accid:selectedaccid}),
            reset: true
        });
    }
});