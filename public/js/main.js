$(document).ready(function () {
    var accCol = new AccountsCollection();
    var transCol = new TransactionsCollection();
    var accList = new AccListView({el:$("#AccountsList"),model:accCol});
    var transList = new TransListView({el:$("#TransactionsList"),model:transCol});
    var accHandler = new AccountsHandler(accCol,transCol);

    var elem = '<div>test</div>';

    accCol.fetch({
        success: function(){
            accList.render();
            $('.popoveracc').popover({animation:true, content:elem, html:true});
        }
    });
    accCol.on('change:selectedacc',accHandler.ChangeSelect);

});