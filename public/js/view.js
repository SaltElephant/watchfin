/**
 * Created by Admin on 24.06.15.
 */

var AccView = Backbone.View.extend({
    model: Account,
    tagName: 'tr',
    template: '',

    events:{
        'click':'select'
    },

    initialize: function() {
        this.template = _.template($('#AccItem').html());
    },

    render: function() {
        this.$el.html(this.template(this.model.attributes));
        //this.$el.addClass('popoveracc');
        return this;
    },

    select : function(){
        $.each(this.$el.siblings(),function(){
            $(this).removeClass('active');
            //$('.popoveracc').not(this).popover('hide');
            //$(this).popover('hide');
        });
        this.$el.addClass('active');
        //this.$el.popover('show');
        this.model.set({selectedacc: true})
    }
});

var AccListView = Backbone.View.extend({
      model: AccountsCollection,
      initialize : function(){
          //this.listenTo(this.model, 'change', this.render);
      },
      render: function() {
        this.$el.html();
        for(var i = 0; i < this.model.length; ++i) {
              var accView = new AccView({model: this.model.at(i)});
              this.$el.append(accView.$el);
              accView.render();
        }
        return this;
      }
});

var TransView = Backbone.View.extend({
    model: Transaction,
    tagName: 'tr',
    template: '',

    events:{
        'click':'select'
    },

    initialize: function() {
        this.template = _.template($('#TransItem').html());
    },

    render: function() {
        formatDate = function(date){
            return date.substr(8,2)+'-'+date.substr(5,2)+'-'+date.substr(0,4);
        };
        this.model.attributes.date = formatDate(this.model.attributes.date);
        this.$el.html(this.template(this.model.attributes));
        return this;
    },

    select : function(){
        $.each(this.$el.siblings(),function(){
            $(this).removeClass('active');
        });
        this.$el.addClass('active');
    }
});

var TransListView = Backbone.View.extend({
    model: TransactionsCollection,
    initialize : function(){
        this.listenTo(this.model, 'reset', this.render);
    },
    render: function() {
        this.$el.html();
        this.$el.empty();
        for(var i = 0; i < this.model.length; ++i) {
            var transView = new TransView({model: this.model.at(i)});
            this.$el.append(transView.$el);
            transView.render();
        }
        return this;
    }
});