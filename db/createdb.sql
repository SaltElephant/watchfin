﻿SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `watchfindb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `watchfindb`;

-- -----------------------------------------------------
-- Table `watchfindb`.`Users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `watchfindb`.`Users` ;

CREATE  TABLE IF NOT EXISTS `watchfindb`.`Users` (
  `user_id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `mail` VARCHAR(255) NOT NULL ,
  `hashedPassword` VARCHAR(255) NOT NULL ,
  `salt` VARCHAR(255) NOT NULL ,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`user_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `watchfindb`.`Currencys`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `watchfindb`.`Currencys` ;

CREATE  TABLE IF NOT EXISTS `watchfindb`.`Currencys` (
  `currency_id` INT NOT NULL AUTO_INCREMENT ,
  `currency_name` VARCHAR(45) NOT NULL ,
  `currency_code` VARCHAR(3) NOT NULL ,
  PRIMARY KEY (`currency_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `watchfindb`.`Accounts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `watchfindb`.`Accounts` ;

CREATE  TABLE IF NOT EXISTS `watchfindb`.`Accounts` (
  `acc_id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `sum` DECIMAL(13,2) NULL ,
  `id_user` INT NOT NULL ,
  `id_currency` INT NOT NULL ,
  PRIMARY KEY (`acc_id`) ,
  INDEX `fk_users` (`id_user` ASC) ,
  INDEX `fk_currencys` (`id_currency` ASC) ,
  CONSTRAINT `fk_users`
    FOREIGN KEY (`id_user` )
    REFERENCES `watchfindb`.`Users` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_currencys`
    FOREIGN KEY (`id_currency` )
    REFERENCES `watchfindb`.`Currencys` (`currency_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `watchfindb`.`Transactions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `watchfindb`.`Transactions` ;

CREATE  TABLE IF NOT EXISTS `watchfindb`.`Transactions` (
  `Transaction_id` BIGINT NOT NULL AUTO_INCREMENT ,
  `id_acc` INT NULL ,
  `trsum` DECIMAL(13,2) NULL ,
  `trname` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`Transaction_id`) ,
  INDEX `fk_account` (`id_acc` ASC) ,
  CONSTRAINT `fk_account`
    FOREIGN KEY (`id_acc` )
    REFERENCES `watchfindb`.`Accounts` (`acc_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `watchfindb`;

-- -----------------------------------------------------
-- Data for table `watchfindb`.`Users`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
INSERT INTO `Users` (`name`, `mail`, `hashedPassword`, `salt`) VALUES ('TestUser','test@test.ru','958847bfbb6d9ea25e9e20f0a72e0d5bd796d5ed','0.7315964815206826');

COMMIT;

-- -----------------------------------------------------
-- Data for table `watchfindb`.`Currencys`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
INSERT INTO `Currencys` (`currency_name`, `currency_code`) VALUES ('Ruble', 'RUB');
INSERT INTO `Currencys` (`currency_name`, `currency_code`) VALUES ('Dollar', 'USD');
INSERT INTO `Currencys` (`currency_name`, `currency_code`) VALUES ('Euro', 'EUR');

COMMIT;

-- -----------------------------------------------------
-- Data for table `watchfindb`.`Accounts`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
INSERT INTO `Accounts` (`name`, `sum`, `id_user`, `id_currency`) VALUES ('Primary acoount','2000','1','1');
INSERT INTO `Accounts` (`name`, `sum`, `id_user`, `id_currency`) VALUES ('Bank acoount','5000','1','2');
INSERT INTO `Accounts` (`name`, `sum`, `id_user`, `id_currency`) VALUES ('Euro acoount','1300','1','3');
COMMIT;

-- -----------------------------------------------------
-- Data for table `watchfindb`.`Transactions`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('1','5000','Salary');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('1','-300','Food');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('1','-1000','Rent');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('2','2500','Deposit1');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('2','1500','Deposit2');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('2','1000','Deposit3');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('3','500','Buy euro');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('3','1000','Buy euro');
INSERT INTO `Transactions` (`id_acc`, `trsum`, `trname`) VALUES ('3','-200','Sell euro');

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
