//var mysql      = require('mysql');
var pg = require('pg');
var config = require('config');

/*var connection = mysql.createConnection({
    host     : config.get('mysql:host'),
    user     : config.get('mysql:user'),
    password : config.get('mysql:pass'),
    database : config.get('mysql:database')
});*/

var conString = "postgres://" + config.get('pg:user') + ":" +
                                config.get('pg:pass') + "@" +
                                config.get('pg:host') + ":" +
                                config.get('pg:port') + "/" +
                                config.get('pg:database');

var client = new pg.Client(conString);

exports.pg = pg;
exports.conString = conString;

exports.query = function(querystr,callback/*err,result*/){
    client.query(querystr,callback);
};

exports.openDatabase = function (callback){
  client.connect(callback);
};

exports.closeDatabase = function(){
  client.end();
};

//for test
/*exports.findUsers = function (callback){
    connection.query('SELECT * FROM Users',callback);
};*/

/*exports.findUser = function (field,value,callback){
    var querystr = "SELECT * FROM Users WHERE " + field + " = '" + value+"'";
    connection.query(querystr,callback*//*err,rows*//*);
};*/
/*
exports.checkPassword = function (password,salt,hashedPassword) {
    return (encryptPassword(password,salt) === hashedPassword);
}
*/

/*exports.addUser = function (name,mail,password,callback){
    var salt = Math.random() + '';
    var hashedPassword = encryptPassword(password,salt);
    var sql = "INSERT INTO Users (`name`, `mail`, `hashedPassword`, `salt`) VALUES (?,?,?,?)";
    var inserts = [name,mail,hashedPassword,salt];
    sql = mysql.format(sql, inserts);
    connection.query(sql,callback*//*err,rows*//*);
};*/
