var User = require('models/user');

module.exports = function(req, res, next) {
  req.user = res.locals.user = null;

  if (!req.session.user) return next();

  User.findUser('user_id',req.session.user, function(err, users) {
    if (err) return next(err);
    req.user = res.locals.user = users.rows[0];
    next();
  });
};