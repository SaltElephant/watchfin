/**
 * Created by Salt Elephant on 10.07.2015.
 */
var Accounts = require('models/accounts');

exports.get = function(req, res, next) {
    var userid = req.session.user;
    if (!userid) return;
    Accounts.findAll(userid,function(err,accounts){
        if (err){
            console.log(err);
            return next(err);
        } else {
            res.json(accounts.rows);
        }
    });
};