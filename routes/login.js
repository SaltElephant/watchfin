/**
 * Created by Admin on 15.04.15.
 */
var HttpError = require('error').HttpError;
var User = require('models/user');

exports.get = function(req, res) {
    if (!req.session.user) {
        res.render('login');
    } else {
        res.redirect('../main');
    }
};

exports.post = function(req, res, next) {
    var mail = req.body.email;
    var password = req.body.password;

    User.authorize(mail, password, function(err, user) {
        if (err) {
            if (err instanceof User.AuthError) {
                return next(new HttpError(403, err.message));
            } else {
                return next(err);
            }
        }
        req.session.user = user.user_id;
        res.send({});
    });

};