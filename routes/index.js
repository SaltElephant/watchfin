var checkAuth = require('middleware/checkAuth');

module.exports = function(app) {
    app.get('/',require('./login').get);
    app.post('/', require('./login').post);
    app.get('/main', checkAuth, require('./main').get);
    app.get('/accounts', require('./accounts').get);//check session inside
    app.get('/transactions',checkAuth, require('./transactions').get);
};

/*module.exports = function(app){
    app.get('/',function(req,res){
       res.render('login');
    });
    app.post('/',function(req,res,next){
        var mail = req.body.Email;
        var password = req.body.Password;
        if(!mail || !password){
            res.status(500).send();
            return false;
        }
        db.findUser('mail',mail,function(err,user) {
            if(err) return next(err);
            if (user) {
                if(db.checkPassword(password,user[0].salt,user[0].hashedPassword)){
                    res.status(200).send();
                    return false;
                }
                res.status(403).send();
            }
        });
    });*/
/*    app.get('/main', function(req, res) {
        res.render('main');
    });*/
/*    app.get('/users', function(req, res) {
        db.findUsers(function(err,users){
            if (err) {
                console.log(err);
                return;
            }
            res.json(users);
        });
    });*/
    /*app.get('/adduser', function(req, res) {
        db.addUser('TestUser','test@test.ru','passss',function(err,result){
            if (err) {
                console.log(err);
                return;
            }
            console.log(result.insertId);
        });
    });*/
//};
