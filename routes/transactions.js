/**
 * Created by Admin on 14.10.15.
 */
var Transactions = require('models/transactions');

exports.get = function(req, res, next) {
    Transactions.findAll(req.query.accid,function(err,transactions){
        if (err){
            console.log(err);
            return next(err);
        } else {
            res.json(transactions.rows);
        }
    });
};