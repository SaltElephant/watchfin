/**
 * Created by Admin on 14.10.15.
 */
var db = require('db');
var sqlformat = require('pg-format');

function findAll(accid,callback){
    var querystr = sqlformat("SELECT transactions.transaction_id AS id, transactions.trname AS name, transactions.trsum AS sum, Category.name AS category, transactions.trdate AS date FROM transactions" +
        " INNER JOIN category ON transactions.id_cat = category.cat_id WHERE id_acc = %L",accid);
    db.query(querystr,callback/*err,result*/);
}

exports.findAll = findAll;