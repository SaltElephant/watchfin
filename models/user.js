/**
 * Created by Admin on 15.04.15.
 */
var crypto = require('crypto');
var db = require('db');
var async = require('async');
var util = require('util');
var sqlformat = require('pg-format');

function findUser(field,value,callback){
    var querystr = sqlformat("SELECT * FROM Users WHERE %I = %L",field,value);
    db.query(querystr,callback/*err,result*/);
}
exports.findUser = findUser;

function encryptPassword(password,salt){
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

function checkPassword(password,salt,hashedPassword) {
    return (encryptPassword(password,salt) === hashedPassword);
}

exports.checkPassword = checkPassword;

exports.addUser = function (name,mail,password,callback){
    var salt = Math.random() + '';
    var hashedPassword = encryptPassword(password,salt);
    var querystr = sqlformat("INSERT INTO Users (`name`, `mail`, `hashedPassword`, `salt`) VALUES (%L,%L,%L,%L)",name,mail,hashedPassword,salt);
    db.query(querystr,callback/*err,result*/);
};

exports.authorize = function(mail, password, callback) {

    async.waterfall([
        function(callback) {
            findUser('mail',mail, function(err,users){
                if (err) {
                    callback(new AuthError("Ошибка базы данных"));
                } else {
                    callback(null, users);
                }
            });
        },
        function(users, callback) {
            if (users.rows.length > 0) {
                if (checkPassword(password,users.rows[0].salt,users.rows[0].hashedPassword)) {
                    callback(null, users.rows[0]);
                } else {
                    callback(new AuthError("Пароль неверен"));
                }
            } else {
                callback(new AuthError("Пользователь не найден"));
            }
        }
    ], callback);
};

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);
    this.message = message;
}
util.inherits(AuthError, Error);
AuthError.prototype.name = 'AuthError';
exports.AuthError = AuthError;