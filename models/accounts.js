/**
 * Created by Salt Elephant on 10.07.2015.
 */
var db = require('db');
var sqlformat = require('pg-format');

function findAll(userid,callback){
    var querystr = sqlformat("SELECT acc_id AS ID, Accounts.name AS Name, Accounts.sum AS Sum, Currencys.currency_code AS Cur FROM Accounts" +
        " INNER JOIN Currencys ON Accounts.id_currency = Currencys.currency_id WHERE id_user = %L",userid);
    db.query(querystr,callback/*err,result*/);
}

exports.findAll = findAll;